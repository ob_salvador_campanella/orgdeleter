#!/bin/bash

#global variables
separator="###############################################\n"
currentDate=$(date +%Y-%m-%d-%T)

# Import Configuration files
source conf/conf-psql.sh

# Import Utils
source libs/print_style.sh
source libs/screenProtection.sh




menu () {
    #  Function: 
    #  - provide a Menu to the users with all the actions posibles
    #  Inputs
    #  - n/a
    #  Outputs
    #  - n/a   

    clear
    printf "\n\n"
    printf "\t Organization Deleter / $currentDate"
    print_style "\n\n ::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n" "success"; 
    print_style "   Database: " "info";printf "\t $psqlDB\n"
    print_style "   User: " "info";printf "\t $psqlUser\n"    
    printf "########################\n"
    printf " M   E   N   U \n"
    printf "########################\n"
    printf "\n ---- Configuration----\n"    
    printf " 1 - Show Psql parameters\n"
    print_style "\n ---- Launch Scripts ----\n" "success";
    printf " 2 - Organization deleter < Using selector >\n"
    printf " 3 - Organization deleter < Inserting ad_org_id >\n"    
    printf " 4 - Organization records finder < Inserting ad_org_id >\n"       
    printf " \n\n"    

    printf 'Please select an option: '
    read DISTR

    case $DISTR in
        1)
            printf "########################\n"
            source libs/parametersManager.sh "conf/conf-psql.sh"
            ;;
        2)
            printf "########################\n"
            delete_org_selector
            ;;           
              
        3)
            printf "########################\n"        
            delete_org_inserting_id
            ;;             

        4)
            printf "########################\n"        
            find_org_records_inserting_id
            ;;        
        *)
            menu
            ;;
    esac    
}
get_organizations(){
    PGPASSWORD=$psqlPass psql -U $psqlUser -d $psqlDB -h $psqlHost -p $psqlPort -f sql-scripts/orgs_to_csv.sql
    OLDIFS=$IFS
    IFS=","
    i=0
    unset orgId
    unset orgName
    unset orgCount
    while read name id count 
    do
        orgId[i]=$id
        orgName[i]=$name
        orgCount[i]=$count
        ((i++))
    done < /tmp/selectOrganizations.csv
    IFS=$OLDIFS    
}

organization_selector (){
    unset array
    for item in "${!orgName[@]}"; do
        array[$item]=${orgName[item]}
    done
    
    pID=$!
    l=$(("${#array[@]}"))
    if [ $((l%2)) -eq 0 ]; then
        l=$((l/2))
    else
        l=$((l/2))
        l=$((l+1))
    fi
    wait $pID
    for((item=0;item<$l;item++)); do
        printf "%-38s ||  %-s\n" "$item - ${array[item]}" "$(($l+$item)) - ${array[item+l]}"
    done
    printf $separator
    printf "Select Organization:"
    read opc
    re='^[0-9]+$'
    if [[ $opc =~ $re ]]; then
        selected=$opc
        organizatioToDelete=${orgId[selected]}     
    else
         selectOrg
    fi
}

delete_org_selector () {
    get_organizations
    organization_selector    
    read -p "Are you sure of delete ${orgName[selected]} organization ? [Yy]: " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        print_style "\n   Removing the organization: " "info"; printf "%-s ||  %-s\n" "${orgName[selected]}" "${orgId[selected]}"
        PGPASSWORD=$psqlPass psql -U $psqlUser -d $psqlDB -h $psqlHost -p $psqlPort -f sql-scripts/delete_org.sql -v v1="'$organizatioToDelete'"
        print_style "   success: " "info"; printf "\t Process Finished\n"    
    fi           
    printf "\n"
}

delete_org_inserting_id () {
    read -p "Please insert the ad_org_id " -n 32 -r
    organizatioToDelete=$REPLY
    printf "\n"    
    read -p "Are you sure of delete organization with the $organizatioToDelete ID? [Yy]: " -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        print_style "\n   Removing the organization with ID: " "info"; printf "%-s\n" "$organizatioToDelete"
        PGPASSWORD=$psqlPass psql -U $psqlUser -d $psqlDB -h $psqlHost -p $psqlPort -f sql-scripts/delete_org.sql -v v1="'$organizatioToDelete'"
        print_style "   success: " "info"; printf "\t Process Finished\n"    
    fi           
    printf "\n"

}

find_org_records_inserting_id(){
    read -p "Please insert the ad_org_id " -n 32 -r
    printf "\n"    
    organizatioToFind=$REPLY
    PGPASSWORD=$psqlPass psql -U $psqlUser -d $psqlDB -h $psqlHost -p $psqlPort -f sql-scripts/find_org_records.sql -v v1="'$organizatioToFind'"
}

core() {
    menu

}

core
CREATE OR REPLACE FUNCTION deleteorgfromtables(org VARCHAR(100))
RETURNS void AS
$BODY$ DECLARE
  current_table RECORD;
  v_Sql VARCHAR(2000);
BEGIN
  RAISE NOTICE '%: deleteorgfromtables function started.', timeofday();
  RAISE NOTICE 'ID: %', org;
  perform ad_disable_triggers();
  SET session_replication_role = replica;

  FOR current_table IN ( 
  select c.table_name
  from information_schema.columns as c 
  join information_schema.tables as t 
  on c.table_schema = 'public' 
  and c.column_name='ad_org_id' 
  and t.table_type='BASE TABLE' 
  and c.table_name = t.table_name
    and t.table_name not in (
    'c_orderline',
    'c_invoiceline',
    'm_transaction',
    'c_orderlinetax',
    'm_inoutline',
    'c_order',
    'obmobc_logclient',
    'c_invoicelinetax',
    'fin_payment',
    'c_invoice',
    'fin_finacc_transaction',
    'fin_payment_schedule',
    'fin_payment_scheduledetail',
    'm_inout',
    'c_ordertax',
    'fin_payment_detail',
    'c_invoicetax',
    'obposl_pendingitem'    
  )
  order by c.table_name) LOOP
	    RAISE NOTICE '%', 'Removing Organization from table: '||current_table.table_name;

      -- Creating sql statement for the current table
	    v_sql := 'DELETE FROM ' || current_table.table_name || ' WHERE ad_org_id = '''|| org || '''';
	    EXECUTE v_sql;
  END LOOP;

  RAISE NOTICE '%: deleteorgfromtables function finished.', timeofday();
  SET session_replication_role = DEFAULT;
  perform ad_enable_triggers();
	
  EXCEPTION
	WHEN OTHERS THEN
    SET session_replication_role = DEFAULT;
    perform ad_enable_triggers();
		RAISE NOTICE '%','Execution error in function';
		RAISE EXCEPTION '%', SQLERRM;
	RETURN;
END;  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- 
select deleteorgfromtables(:v1);
drop function  deleteorgfromtables(org VARCHAR(100));
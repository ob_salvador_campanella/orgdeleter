CREATE OR REPLACE FUNCTION findorgrecords(org VARCHAR(100))
RETURNS void AS
$BODY$ DECLARE
  current_table RECORD;
  v_Sql VARCHAR(2000);
  result VARCHAR(30);
BEGIN
  RAISE NOTICE '%: findorgrecords function started.', timeofday();
  RAISE NOTICE 'ID: %', org;

  FOR current_table IN ( 
  select c.table_name
  from information_schema.columns as c 
  join information_schema.tables as t 
  on c.table_schema = 'public' 
  and c.column_name='ad_org_id' 
  and t.table_type='BASE TABLE' 
  and c.table_name = t.table_name
  and t.table_name not in (
    'c_orderline',
    'c_invoiceline',
    'm_transaction',
    'c_orderlinetax',
    'm_inoutline',
    'c_order',
    'obmobc_logclient',
    'c_invoicelinetax',
    'fin_payment',
    'c_invoice',
    'fin_finacc_transaction',
    'fin_payment_schedule',
    'fin_payment_scheduledetail',
    'm_inout',
    'c_ordertax',
    'fin_payment_detail',
    'c_invoicetax',
    'obposl_pendingitem'    
  )
  order by c.table_name) LOOP

      -- Creating sql statement for the current table
	    v_sql := 'SELECT 1 FROM ' || current_table.table_name || ' WHERE ad_org_id = '''|| org || ''' limit 1';
	    EXECUTE v_sql INTO result;
    IF result = '1' THEN
      RAISE NOTICE '%', current_table.table_name;
    END IF;
  END LOOP;

  RAISE NOTICE '%: findorgrecords function finished.', timeofday();
	
  EXCEPTION
	WHEN OTHERS THEN

		RAISE NOTICE '%','Execution error in function';
		RAISE EXCEPTION '%', SQLERRM;
	RETURN;
END;  $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- 

select findorgrecords(:v1);
drop function  findorgrecords(org VARCHAR(100));
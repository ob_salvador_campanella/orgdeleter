#!/bin/bash
paramsFile=$1
separator=" ############\n"
print_style () {
    : '
    Show messages with a color descriptor prefix.

    print_style "      info: " "info";    printf "\t info msg\n"
    print_style "   success: " "success"; printf "\t success msg\n"
    print_style "   warning: " "warning"; printf "\t warning msg\n"
    print_style "    danger: " "danger";  printf "\t danger msg\n"
    '
    if [ "$2" == "info" ] ; then
        COLOR="96m"
    elif [ "$2" == "success" ] ; then
        COLOR="92m"
    elif [ "$2" == "warning" ] ; then
        COLOR="93m"
    elif [ "$2" == "danger" ] ; then
        COLOR="91m"
    else #default color
        COLOR="0m"
    fi

    STARTCOLOR="\e[$COLOR"
    ENDCOLOR="\e[0m"

    printf "$STARTCOLOR%b$ENDCOLOR" "$1"
}

menu () {
    printParameters
    printf "\n\n"
    printf $separator
    printf "     M   E   N   U \n"
    printf $separator
    printf " 1 - Edit Parameters \n"
    printf " r - return to the launcher"
    printf "\n"
    printf '\n Please select an option: '
    read DISTR

    case $DISTR in
        1)
            printf $separator
            nano $paramsFile
            source $paramsFile
            menu
            ;;        
        r)
            source run.sh
            ;;              
        *)
            core
            ;;
    esac    
}

printParameters () {
    clear
    print_style " \- Current Parameters: -\\ \n\n" "success";
    print_style "$(cat $paramsFile)" "warning";
    
}

core () {
   menu
}

core
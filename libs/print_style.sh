#!/bin/bash
print_style () {
    # print_style "   info: " "info";printf "\t info msg\n"
    # print_style "   success: " "success"; printf "\t success msg\n"
    # print_style "   warning: " "warning"; printf "\t warning msg\n"
    # print_style "    danger: " "danger";  printf "\t danger msg\n"
    if [ "$2" == "info" ] ; then
        COLOR="96m"
    elif [ "$2" == "success" ] ; then
        COLOR="92m"
    elif [ "$2" == "warning" ] ; then
        COLOR="93m"
    elif [ "$2" == "danger" ] ; then
        COLOR="91m"
    else #default color
        COLOR="0m"
    fi

    STARTCOLOR="\e[$COLOR"
    ENDCOLOR="\e[0m"

    printf "$STARTCOLOR%b$ENDCOLOR" "$1"
}
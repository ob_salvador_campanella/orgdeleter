
#!/bin/bash

# Screen protection
screen_protection () {
if [ "$STY" == " " ] || [ -z "$STY" ]; then 
    print_style "\n    Error: Is mandatory run this Script into Screen session \n\n" "danger";
    printf "  execute Screen command and then this script ...\n\n"
else
    $1
fi }
